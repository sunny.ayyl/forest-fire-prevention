import requests as req
import pandas as pd
from io import StringIO
import re
import json
import googlemaps

gmaps = googlemaps.Client("AIzaSyBnvexb5fYN9HK4xy-c8uFun3Vj3nOy_no")
time_list=[]
loc_per={}
data=pd.read_csv(StringIO(req.get("https://data.weather.gov.hk/weatherAPI/hko_data/regional-weather/latest_1min_humidity.csv").text))
print(data)
try:
    print(range(len(data)))
    for time in range(len(data)):
        print(time)
        date=str(data["Date time"][time])
        year=re.search(r"\b\w{4}",date).group(0)
        date=date.replace(year,"")
        month=re.search(r"\b\w{2}",date).group(0)
        date=date.replace(month,"")
        day=re.search(r"\b\w{2}",date).group(0)
        date=date.replace(day,"")
        hour=re.search(r"\b\w{2}",date).group(0)
        date=date.replace(hour,"")
        mins=re.search(r"\b\w{2}",date).group(0)
        date=date.replace(mins,"")
        print("{}/{}/{},{}:{}".format(day,month,year, hour, mins))
except :
    pass
data_=data.columns[0]
data.drop(data_,inplace=True,axis=1)
data.insert(0,"Date time",time_list)
for time in range(len(data)):
    location = data["Automatic Weather Station"][time]
    location = gmaps.geocode(location)
    percent = data["Relative Humidity(percent)"][time]
    loc_per[location]=int(percent)
print(loc_per)
open("data.json", "w").write(json.dumps(loc_per))
