import requests as req
import pandas as pd
from io import StringIO
import re
import json
import googlemaps

gmaps = googlemaps.Client("AIzaSyDPAvCAMfCgZrT8seUmAVtfwbvOC-0Ddmk")
time_list=[]
loc_per={}
data=pd.read_csv(StringIO(req.get("https://data.weather.gov.hk/weatherAPI/hko_data/regional-weather/latest_1min_humidity.csv").text))
print(data)

for time in range(len(data)):
    location = data["Automatic Weather Station"][time]
    loc_g = gmaps.geocode(location)[0]["geometry"]["location"]
    print(str(loc_g)+"\n\n")
    location = "var loc_=function (){const loc={lat:parseFloat("+str(loc_g["lat"])+"),lng:parseFloat("+str(loc_g["lng"])+")};return loc}"
        
    percent = data["Relative Humidity(percent)"][time]
    loc_per[location]=int(percent)
print(loc_per)
open("data.json", "w").write(json.dumps(loc_per))
